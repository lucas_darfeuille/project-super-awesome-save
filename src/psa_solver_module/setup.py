from setuptools import setup, Extension

from setuptools.command.build_py import build_py as _build_py

class build_py(_build_py):
    def run(self):
        self.run_command("build_ext")
        return super().run()

module_psa = Extension('_psa_solver', sources=['psa_solver.i', './cpp/solver.cpp']
    , include_dirs = ['./armanpy_python3/']
    , libraries=['m','z', 'armadillo'] 
    , swig_opts = ["-c++", "-Wall", "-I.", "-I./armanpy_python3/"])


setup(name='psa_solverPackage', version='1.0',
       py_modules = ['psa_solver', 'init_and_utilities', 'preprocessing'],
       cmdclass = {'build_py' : build_py},       
       options = {"build_ext": {"inplace": False}},
 description="This package contains a module for computing a 2D time-dependent Shrödinger's equation Harmonic Solutions",
  ext_modules=[module_psa])